<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('login', 'RegistroController@login');
Route::apiResource('tema', 'TopicsController');
Route::apiResource('registro', 'RegistroController');
Route::apiResource('consulta', 'ForoController');
Route::apiResource('consulta1', 'ComentariosController');
Route::apiResource('consulta2', 'CliptocallController');
Route::apiResource('respuesta', 'RespuestasController');

