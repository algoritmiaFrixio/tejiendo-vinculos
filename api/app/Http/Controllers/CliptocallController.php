<?php

namespace App\Http\Controllers;

use App\cliptocall;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class CliptocallController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      try {
          $telefono = cliptocall::all();
          return response()->json(['data' => $telefono], 200);
      } catch (ModelNotFoundException $e){
          return response()->json(["message" => $e->getMessage()], 500);
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       try {
           $telefono = new cliptocall();
           $telefono->nombre = $request->nombre;
           $telefono->correo = $request->correo;
           $telefono->recidencia = $request->recidencia;
           $telefono->saveOrFail();
           $data = cliptocall::all();
           return response()->json(['message' => "Agregado correctamente", 'datos' => $data], 201);
       } catch (ModelNotFoundException $e) {
           return response()->json(["message" => $e->getMessage()], 500);
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\cliptocall  $cliptocall
     * @return \Illuminate\Http\Response
     */
    public function show(cliptocall $cliptocall)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\cliptocall  $cliptocall
     * @return \Illuminate\Http\Response
     */
    public function edit(cliptocall $cliptocall)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\cliptocall  $cliptocall
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, cliptocall $cliptocall)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\cliptocall  $cliptocall
     * @return \Illuminate\Http\Response
     */
    public function destroy(cliptocall $cliptocall)
    {
        //
    }
}
