<?php

namespace App\Http\Controllers;

use App\registro;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegistroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $foros = registro::all();
            return response()->json(['data' => $foros], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        try {
            $register = new  registro();
            $register->nombre = $request->nombre;
            $register->fecha_nacimiento = $request->fecha_nacimiento;
            $register->telefono = $request->telefono;
            $register->ciudad = $request->ciudad;
            $register->tipo_documento = $request->tipo_documento;
            $register->numero_documento = $request->numero_documento;
            $register->correo = $request->correo;
            $register->contrasena = Hash::make($request->contrasena);
            $register->saveOrFail();
            return response()->json(['message' => "Agregado correctamente", 'datos' => $register], 201);
        } catch (ModelNotFoundException $e) {
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\registro $registro
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\registro $registro
     * @return \Illuminate\Http\Response
     */
    public function edit(registro $registro)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\registro $registro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, registro $registro)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\registro $registro
     * @return \Illuminate\Http\Response
     */
    public function destroy(registro $registro)
    {
        //
    }

    public function login(Request $request)
    {
        try {
            $data = registro::where('correo', $request->correo)->firstOrFail();
            if (Hash::check($request->contrasena, $data->contrasena)) {
                return response()->json(['message' => 'Sesión iniciada correctamente', 'datos' => $data], 200);
            }
            return response()->json(['message' => 'Datos incorrectos'], 403);
        } catch (ModelNotFoundException $e) {
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }
}
