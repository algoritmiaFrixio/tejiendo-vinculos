<?php

namespace App\Http\Controllers;

use App\Comentarios;
use App\Foro;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Throwable;

class ComentariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $comentarios = Foro::all();
                foreach ($comentarios as $comentario) {
                    $comentario->respuestas;
                }
            return response()->json(['data' => $comentarios], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws Throwable
     */
    public function store(Request $request)
    {
        try {
            $descripcion = new Comentarios();
            $descripcion->nombre = $request->nombre;
            $descripcion->correo = $request->correo;
            $descripcion->comentario = $request->comentario;
            $descripcion->id_foro = $request->id_foro;
            $descripcion->saveOrFail();
            $data = Comentarios::where('id_foro', $request->id_foro)->get();
            foreach ($data as $comentario) {
                $comentario->respuestas;
            }
            return response()->json(['message' => "Agregado correctamente", 'datos' => $data], 201);
        } catch (ModelNotFoundException $e) {
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Comentarios $comentarios
     * @return void
     */
    public function show(Comentarios $comentarios)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Comentarios $comentarios
     * @return void
     */
    public function edit(Comentarios $comentarios)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Comentarios $comentarios
     * @return void
     */
    public function update(Request $request, Comentarios $comentarios)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Comentarios $comentarios
     * @return Response
     */
    public function destroy(Comentarios $comentarios)
    {
        //
    }
}
