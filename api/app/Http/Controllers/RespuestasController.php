<?php

namespace App\Http\Controllers;

use App\Foro;
use App\respuestas;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Throwable;

class RespuestasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $respuestas = respuestas::where('id_comentario', $request->id_comentario)->get();
            return response()->json(['data' => $respuestas], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws Throwable
     */
    public function store(Request $request)
    {
        try {
            $respuesta = new respuestas();
            $respuesta->nombre = $request->nombre;
            $respuesta->respuesta = $request->respuesta;
            $respuesta->id_comentario = $request->id_comentario;
            $respuesta->saveOrFail();
            $foros = Foro::all();
            foreach ($foros as $foro) {
                foreach ($foro->comentarios as $comentario) {
                    $comentario->respuestas;
                }
            }
            return response()->json(['message' => "Agregado correctamente", 'datos' => $foros], 201);
        } catch (ModelNotFoundException $e) {
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\respuestas $respuestas
     * @return Response
     */
    public function show(respuestas $respuestas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\respuestas $respuestas
     * @return Response
     */
    public function edit(respuestas $respuestas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param \App\respuestas $respuestas
     * @return Response
     */
    public function update(Request $request, respuestas $respuestas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\respuestas $respuestas
     * @return Response
     */
    public function destroy(respuestas $respuestas)
    {
        //
    }
}
