<?php

namespace App\Http\Controllers;

use App\topics;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class TopicsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $temas = topics::where('id_tema', $request->id_tema)->get();
            return response()->json(['data' => $temas], 200);
        } catch (ModelNotFoundException $e){
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            dd($request->file('files'));
            $tema = new topics();
            $tema->tema = $request->tema_semana;
            $tema->descripcion = $request->descripcion_tema;
            $tema =time(). $request->file('files')->getClientOriginalName();
            $request->file('files')->move('/images/temas'. $tema->id_tema, $tema);
            $imagen_tema [] = 'http://localhost:8000/api//images/temas' . $tema->id_tema . '/' . $tema;
            $tema->saveOrFail();
            $data = topics::where('id_tema', $request->id_tema)->get();
            return response()->json(['message' => "Agregado correctamente", 'datos' => $data], 201);
        } catch (ModelNotFoundException $e) {
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\topics  $topics
     * @return \Illuminate\Http\Response
     */
    public function show(topics $topics)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\topics  $topics
     * @return \Illuminate\Http\Response
     */
    public function edit(topics $topics)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\topics  $topics
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, topics $topics)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\topics  $topics
     * @return \Illuminate\Http\Response
     */
    public function destroy(topics $topics)
    {
        //
    }
}
