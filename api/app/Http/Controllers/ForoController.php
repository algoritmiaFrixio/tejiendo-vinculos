<?php

namespace App\Http\Controllers;

use App\Foro;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse as JsonResponseAlias;
use Illuminate\Http\Request;

class ForoController extends Controller
{
    /**
     * @return JsonResponseAlias
     */
    public function index()
    {
        try {
            $foros = Foro::all();
            foreach ($foros as $foro) {
                foreach ($foro->comentarios as $comentario) {
                    $comentario->respuestas;
                }
            }
//            $foros = Foro::all()->count();
            return response()->json(['data' => $foros], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }

    /**
     *
     */
    public function create()
    {
        //
    }

    /**
     * @param Request $request
     * @return JsonResponseAlias
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        try {
            $foro = new Foro();
            $foro->nombre = $request->nombre;
            $foro->tema = $request->title;
            $foro->descripcion = $request->description;
            $foro->saveOrFail();
            $data = Foro::all();
            return response()->json(['message' => "Agregado correctamente", 'datos' => $data], 201);
        } catch (ModelNotFoundException $e) {
            return response()->json(["message" => $e->getMessage()], 500);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param Foro $foro
     * @return \Illuminate\Http\Response
     */
    public function show(Foro $foro)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Foro $foro
     * @return \Illuminate\Http\Response
     */
    public function edit(Foro $foro)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Foro $foro
     * @return JsonResponseAlias
     */
    public function update(Request $request, Foro $foro)
    {
        if ($request->json()) {
            try {
                $foro = Foro::where('id', $request->id)->firstOrFail();
                $foro->tema = $request->titulo_tema;
                $foro->descripcion = $request->descripcion_tema;
                $foro->saveOrFail();
                $foros = Foro::all();
                return response()->json(['message' => 'Cambio de estado correctamente', 'foros' => $foros], 200);
            } catch (ModelNotFoundException  $exception) {
                return response()->json(['message' => 'Ocurrio un error en la solicitud'], 500);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param Foro $foro
     * @param Request $request
     * @return JsonResponseAlias
     */
    public function destroy(Foro $foro, Request $request)
    {

        if ($request->isJson()) {
            try {
                $foro = Foro::where('id', $request->id)->firstOrFail();
                $foro->delete();
                $foros = Foro::all();
                return response()->json(['message' => 'Eliminado correctamente', 'data' => $foros], 200);
            } catch (ModelNotFoundException  $exception) {
                return response()->json(['message' => $exception->getMessage()], 500);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }
}
