<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentarios extends Model
{
    public function respuestas()
    {
        return $this->hasMany(respuestas::class, 'id_comentario');
    }
}
