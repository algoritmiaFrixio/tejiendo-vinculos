<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Foro extends Model
{


    public function comentarios()
    {
        return $this->hasMany(Comentarios::class, 'id_foro');
    }
}
