<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registros', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->string('fecha_nacimiento');
            $table->bigInteger('telefono');
            $table->string('ciudad');
            $table->string('role')->default('user');
            $table->string('tipo_documento');
            $table->bigInteger('numero_documento');
            $table->string('correo', 90)->unique();
            $table->string('contrasena', 60);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registros');
    }
}
