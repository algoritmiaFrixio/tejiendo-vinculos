<?php

use Illuminate\Database\Seeder;

class RegistrosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       \Illuminate\Support\Facades\DB::table('registros')->insert([
           'nombre' => 'Ana Isabel Hurtado Cuellar',
           'fecha_nacimiento' => '02/11/1996',
           'telefono' => 3186260552,
           'ciudad' => 'Buenaventura',
           'role' => 'administrador',
           'tipo_documento' => 'C.C',
           'numero_documento' => '1107509718',
           'correo' => 'isahucu5@gmail.com',
           'contrasena' => \Illuminate\Support\Facades\Hash::make('dobby2018')
       ]);

        \Illuminate\Support\Facades\DB::table('registros')->insert([
            'nombre' => 'Jose Luis Gonzalez Ciro',
            'fecha_nacimiento' => '09/28/1997',
            'telefono' => 3165305948,
            'ciudad' => 'Cartago',
            'tipo_documento' => 'C.C',
            'numero_documento' => '1006318224',
            'correo' => 'joseciro1377@gmail.com',
            'contrasena' => \Illuminate\Support\Facades\Hash::make('jose123456')
        ]);
    }
}
