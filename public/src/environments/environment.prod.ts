export const environment = {
  production: true,
  API_ENDPOINT: 'https://api.reconocimientojusticiaydesarrollo.org/api'
};
