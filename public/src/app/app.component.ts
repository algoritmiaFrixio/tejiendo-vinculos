import { Component } from '@angular/core';
import { Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'public';
  constructor(private router: Router) {
    if (localStorage.getItem('user') !== null) {
      if (JSON.parse(atob(localStorage.getItem('user'))).role === 'administrador') {
        router.navigate(['/admin']);
      }
    }
  }

}
