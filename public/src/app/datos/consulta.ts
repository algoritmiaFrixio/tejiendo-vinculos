export interface Consulta {
  id?: number;
  tema?: string;
  descripcion?: string;
  created_at?: string;
  update_at?: string;
}

export interface Consulta1 {
  id?: number;
  nombre?: string;
  correo?: string;
  comentario?: string;
  created_at?: string;
  update_at?: string;
}

export interface  Consulta2 {
  id?: number;
  nombre?: string;
  correo?: string;
  recidencia?: string;
  created_at?: string;
  update_at?: string;
}

export interface  Respuesta {
  id?: number;
  nombre?: string;
  respuesta?: string;
  created_at?: string;
  update_at?: string;
}
