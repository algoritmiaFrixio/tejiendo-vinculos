import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';

declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}

export const ROUTES: RouteInfo[] = [
  {path: '/admin', title: 'Dashboard', icon: 'dashboard', class: ''},
  {path: '/admin/tema', title: 'temas', icon: 'assignment', class: ''},
  {path: '/admin/foto', title: 'Fotos', icon: 'perm_media', class: ''},
  {path: '/admin/video', title: 'Video', icon: 'shop_two', class: ''}
];

@Component({
  selector: 'app-admin-main',
  templateUrl: './admin-main.component.html',
  styleUrls: ['./admin-main.component.scss']
})
export class AdminMainComponent implements OnInit {
  menuItems: any[];

  constructor(private toastr: ToastrService, private router: Router, private  route: ActivatedRoute) {
    if (localStorage.getItem('user') === null) {
      router.navigate(['/']);
    } else if (JSON.parse(atob(localStorage.getItem('user'))).role !== 'administrador') {
      router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }

  closesession() {
    if (localStorage.getItem('user') !== null) {
      localStorage.removeItem('user');
      this.router.navigate(['/public']);
      this.toastr.success('', 'La sesion se ha finalizado');
    }
  }
}
