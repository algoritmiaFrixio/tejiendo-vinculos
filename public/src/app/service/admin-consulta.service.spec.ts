import { TestBed } from '@angular/core/testing';

import { AdminConsultaService } from './admin-consulta.service';

describe('AdminConsultaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdminConsultaService = TestBed.get(AdminConsultaService);
    expect(service).toBeTruthy();
  });
});
