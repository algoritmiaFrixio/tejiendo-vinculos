import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConsultaforoService {

  constructor(private http: HttpClient) {
  }

  save(consulta): Observable<any> {
    return this.http.post<any>(`${environment.API_ENDPOINT}/consulta`, consulta,
      {
        headers: {'Content-Type': 'application/json'},
        observe: 'response'
      }).pipe(map((response: HttpResponse<any>) => response.body));
  }

  get(): Observable<any> {
    return this.http.get<any>(`${environment.API_ENDPOINT}/consulta`,
      {
        headers: {'Content-Type': 'application/json'},
        observe: 'response'
      }).pipe(map((response: HttpResponse<any>) => response.body));
  }

  save1(consulta1: any, id): Observable<any> {
    consulta1.id_foro = id;
    return this.http.post<any>(`${environment.API_ENDPOINT}/consulta1`, consulta1,
      {
        headers: {'Content-Type': 'application/json'},
        observe: 'response'
      }).pipe(map((response: HttpResponse<any>) => response.body));
  }

  get1(id): Observable<any> {
    return this.http.get<any>(`${environment.API_ENDPOINT}/consulta1?id_foro=${id}`,
      {
        headers: {'Content-Type': 'application/json'},
        observe: 'response'
      }).pipe(map((response: HttpResponse<any>) => response.body));
  }

  savedate(consulta2): Observable<any> {
    return this.http.post<any>(`${environment.API_ENDPOINT}/consulta2`, consulta2,
      {
        headers: {'Content-Type': 'application/json'},
        observe: 'response'
      }).pipe(map((response: HttpResponse<any>) => response.body));
  }

  savedataregister(registro): Observable<any> {
    return this.http.post<any>(`${environment.API_ENDPOINT}/registro`, registro,
      {
        headers: {'Content-Type': 'application/json'},
        observe: 'response'
      }).pipe(map((response: HttpResponse<any>) => response.body));
  }

  savedatalogin(login): Observable<any> {
    return this.http.post<any>(`${environment.API_ENDPOINT}/login`, login,
      {
        headers: {'Content-Type': 'application/json'},
        observe: 'response'
      }).pipe(map((response: HttpResponse<any>) => response.body));
  }

  gettheme(id): Observable<any> {
    return this.http.get<any>(`${environment.API_ENDPOINT}/tema?id_foro=${id}`,
      {
        headers: {'Content-Type': 'application/json'},
        observe: 'response'
      }).pipe(map((response: HttpResponse<any>) => response.body));
  }

  public adddate(theme) {
    const form = new FormData();
    form.append('tema_semana', theme.value.tema_semana);
    form.append('descripcion_tema', theme.value.descripcion_tema);
    return this.http.post(`${environment.API_ENDPOINT}/tema`, form, {
      observe: 'response'
    }).pipe(map((response: HttpResponse<any>) => response.body));
  }
}

