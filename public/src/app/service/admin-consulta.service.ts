import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdminConsultaService {

  constructor(private http: HttpClient) {
  }

  public DeleteTheme(id): Observable<any> {
    return this.http.delete<any>(`${environment.API_ENDPOINT}/consulta/${id}?id=${id}`,
      {
        headers: {'Content-Type': 'application/json'},
        observe: 'response'
      }).pipe(map((response: HttpResponse<any>) => response.body));
  }

  public editSolicitud(id, data): Observable<any> {
    data.id = id;
    // tslint:disable-next-line:max-line-length
    return this.http.put<any>(`${environment.API_ENDPOINT}/consulta/${id}&id=${id}&titulo_tema=${data.titulo_tema}&descripcion_tema=${data.descripcion} `, data,
      {
        headers: {'Content-Type': 'application/json'}
      }).pipe(map((response: any) => response));
  }

  SaveReply(respuesta: any, id): Observable<any> {
    respuesta.id_comentario = id;
    return this.http.post<any>(`${environment.API_ENDPOINT}/respuesta`, respuesta,
      {
        headers: {'Content-Type': 'application/json'},
        observe: 'response'
      }).pipe(map((response: HttpResponse<any>) => response.body));
  }

  get(): Observable<any> {
    return this.http.get<any>(`${environment.API_ENDPOINT}/consulta`,
      {
        headers: {'Content-Type': 'application/json'},
        observe: 'response'
      }).pipe(map((response: HttpResponse<any>) => response.body));
  }

  GetReply(id): Observable<any> {
    return this.http.get<any>(`${environment.API_ENDPOINT}/respuesta?id_comentario=${id}`,
      {
        headers: {'Content-Type': 'application/json'},
        observe: 'response'
      }).pipe(map((response: HttpResponse<any>) => response.body));
  }
}
