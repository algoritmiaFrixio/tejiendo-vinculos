import { TestBed } from '@angular/core/testing';

import { ConsultaForoService } from './consulta-foro.service';

describe('ConsultaForoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConsultaForoService = TestBed.get(ConsultaForoService);
    expect(service).toBeTruthy();
  });
});
