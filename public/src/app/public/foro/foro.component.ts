import {Component, Inject, OnInit} from '@angular/core';
import {ConsultaforoService} from '../../service/consulta-foro.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-foro',
  templateUrl: './foro.component.html',
  styleUrls: ['./foro.component.scss']
})
export class ForoComponent implements OnInit {
  showconsult = [];
  constructor(public dialog: MatDialog, private consultaservice: ConsultaforoService) {
    consultaservice.get().subscribe(response => {
      this.showconsult = response.data;
    });
  }



  ngOnInit() {
  }


}


