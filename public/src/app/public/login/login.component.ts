import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {ConsultaforoService} from '../../service/consulta-foro.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public firstFormGroup: FormGroup;

  constructor(public formBuilder: FormBuilder, public Toars: ToastrService, public  consultaservice: ConsultaforoService,
              public dialog: MatDialog,
              public dialogRef: MatDialogRef<LoginComponent>,
              @Inject(MAT_DIALOG_DATA) public data, private router: Router) {
    if (localStorage.getItem('user') !== null) {
      this.router.navigate(['/public']);
    }

    this.firstFormGroup = this.formBuilder.group({
      correo: new FormControl('', [Validators.pattern(/^\w+([\.\+\-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/)]),
      contrasena: new FormControl('', Validators.required),
    });
  }

  ngOnInit() {
    document.getElementsByClassName('mat-dialog-container')[0].classList.add('login');
  }

  onNoClick(): void {
    this.dialogRef.close(true);
  }

  public auth(form: FormGroup) {
    this.consultaservice.savedatalogin(form.value).subscribe((response: any) => {
      localStorage.setItem('user', btoa(JSON.stringify(response.datos)));
      if (JSON.parse(atob(localStorage.getItem('user'))).role === 'administrador') {
        this.router.navigate(['/admin']);
      }
      this.Toars.success('', 'Se ha iciado sesion correctamente');
      this.data = false;
      this.dialogRef.close(this.data);
    }, error => {
      this.Toars.error(error, 'El usuario o la contraseña no son correctos');
    });
  }


}
