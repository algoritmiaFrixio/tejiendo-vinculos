import {Component, OnInit} from '@angular/core';
import {AlertainiciosesionComponent} from '../alertainiciosesion/alertainiciosesion.component';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-galeria',
  templateUrl: './galeria.component.html',
  styleUrls: ['./galeria.component.scss']
})
export class GaleriaComponent implements OnInit {
  public images: string[] = [
    'https://api.reconocimientojusticiaydesarrollo.org/images/temas/1.png',
    'https://api.reconocimientojusticiaydesarrollo.org/images/temas/2.png',
    'https://api.reconocimientojusticiaydesarrollo.org/images/temas/3.png',
    'https://api.reconocimientojusticiaydesarrollo.org/images/temas/4.png',
    'https://api.reconocimientojusticiaydesarrollo.org/images/temas/5.png',
    'https://api.reconocimientojusticiaydesarrollo.org/images/temas/6.png',
    'https://api.reconocimientojusticiaydesarrollo.org/images/temas/7.jpeg',
    'https://api.reconocimientojusticiaydesarrollo.org/images/temas/8.jpeg'
  ];
  public tmpImages: string[] = [];
  public videos: string[] = [
    'https://api.reconocimientojusticiaydesarrollo.org/video/1.mp4',
    'https://api.reconocimientojusticiaydesarrollo.org/video/2.mp4',
    'https://api.reconocimientojusticiaydesarrollo.org/video/3.mp4',
    'https://api.reconocimientojusticiaydesarrollo.org/video/4.mp4',
  ];

  constructor(public dialog: MatDialog) {
  }

  ngOnInit() {
  }

  openDialogalert(index): void {
    this.tmpImages = this.images;
    this.tmpImages = this.tmpImages.splice(index, this.tmpImages.length);
    for (let i = 0; i < this.images.length; i++) {
      this.tmpImages.push(this.images[i]);
      if (i === index) {
        i = this.images.length + 1;
        return;
      }
    }
    this.images = [
      'https://api.reconocimientojusticiaydesarrollo.org/images/temas/1.png',
      'https://api.reconocimientojusticiaydesarrollo.org/images/temas/2.png',
      'https://api.reconocimientojusticiaydesarrollo.org/images/temas/3.png',
      'https://api.reconocimientojusticiaydesarrollo.org/images/temas/4.png',
      'https://api.reconocimientojusticiaydesarrollo.org/images/temas/5.png',
      'https://api.reconocimientojusticiaydesarrollo.org/images/temas/6.png',
      'https://api.reconocimientojusticiaydesarrollo.org/images/temas/7.jpeg',
      'https://api.reconocimientojusticiaydesarrollo.org/images/temas/8.jpeg'
    ];
    const dialogRef = this.dialog.open(AlertainiciosesionComponent, {
      width: '45vw',
      height: '60vh',
      disableClose: true,
      data: this.tmpImages
    });
  }
}
