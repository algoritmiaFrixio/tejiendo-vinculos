import {Component, Inject, OnInit} from '@angular/core';
import {Consulta1, Respuesta} from '../../datos/consulta';
import {ConsultaforoService} from '../../service/consulta-foro.service';
import {ActivatedRoute} from '@angular/router';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {AlertainiciosesionComponent} from '../alertainiciosesion/alertainiciosesion.component';
import {AdminConsultaService} from '../../service/admin-consulta.service';

export interface Con {
  nombre?: string;
  tema?: string;
  descripcion?: string;
}



@Component({
  selector: 'app-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.scss']
})
export class DescriptionComponent implements OnInit {
  consul;
  id;
  showcomments = [];
  fecha = '';
  showReply: Respuesta[] = [];


  constructor(private consultaservice: ConsultaforoService, private route: ActivatedRoute, public dialog: MatDialog,
              private adminConsulta: AdminConsultaService) {
    this.route.params.subscribe(params => {
      const id = +params.id;
      this.id = id;
      consultaservice.get().subscribe(response => {
        response.data.forEach(e => {
          this.consul = e.id === id ? e : this.consul;
        });
        this.showcomments = this.consul.comentarios;
      });
    });

  }

  openDialogcomentario(): void {
    const dialogRef = this.dialog.open(CometaryComponent, {
      width: '45vw',
      height: '75vh',
      disableClose: true,
      data: {id: this.id, con: this.showcomments}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.showcomments = result;
      }
    });
  }

  opendialog() {
    if (localStorage.getItem('user') !== null) {
      this.openDialogcomentario();
    } else {
      this.openDialogalert();
    }
  }

  ngOnInit() {
    this.fecha = new Date().getDate() + '/' + (new Date().getMonth() + 1) + '/' + new Date().getFullYear();
  }

  openDialogalert(): void {
    const dialogRef = this.dialog.open(AlertainiciosesionComponent, {
      width: '35vw',
      height: '45vh',
    });
  }
}


@Component({
  selector: 'app-dialog-comentario',
  templateUrl: 'comentario.html',
  styleUrls: ['./comentario.css']
})
export class CometaryComponent implements OnInit {
  id;
  consulta1: Consulta1 = {
    nombre: '',
    correo: '',
    comentario: '',
  };

  constructor(public dialogRef: MatDialogRef<CometaryComponent>, private consultaservice: ConsultaforoService,
              @Inject(MAT_DIALOG_DATA) public data, private route: ActivatedRoute, private toastr: ToastrService) {
    this.consulta1.nombre = JSON.parse(atob(localStorage.getItem('user'))).nombre;
    this.consulta1.correo = JSON.parse(atob(localStorage.getItem('user'))).correo;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  save1date() {
    this.consultaservice.save1(this.consulta1, this.data.id).subscribe(response => {
      this.data.con = response.datos;
      this.consulta1.comentario = '';
      this.dialogRef.close(this.data.con);
      this.toastr.success('Se ha agregado exitosamente', 'Comentario agregado con exito');
    }, error1 => {
      this.toastr.error(error1, 'Ha ocurrido un error');
    });
  }

  ngOnInit(): void {
    document.getElementsByClassName('mat-dialog-container')[0].classList.add('register');
  }
}
