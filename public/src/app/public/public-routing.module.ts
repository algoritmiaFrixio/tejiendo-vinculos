import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AfroplayComponent} from './afroplay/afroplay.component';
import {ProgramacionComponent} from './programacion/programacion.component';
import {ContactoComponent} from './contacto/contacto.component';
import {CursoVirtualComponent} from './curso-virtual/curso-virtual.component';
import {GaleriaComponent} from './galeria/galeria.component';
import {QuienessomosComponent} from './quienessomos/quienessomos.component';
import {DescriptionComponent} from './description/description.component';
import {ForoComponent} from './foro/foro.component';
import {IndexComponent} from './index/index.component';


const routes: Routes = [
  {
    path: '',
    component: IndexComponent
  },
  {
    path: 'foro',
    component: ForoComponent
  },
  {
    path: 'description/:id',
    component: DescriptionComponent
  },
  {
    path: 'quienessomos',
    component: QuienessomosComponent
  },
  {
    path: 'galeria',
    component: GaleriaComponent
  },
  {
    path: 'cursovirtual',
    component: CursoVirtualComponent
  },
  {
    path: 'contacto',
    component: ContactoComponent
  },
  {
    path: 'programacion',
    component: ProgramacionComponent
  },
  {
    path: 'afroplay',
    component: AfroplayComponent
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
