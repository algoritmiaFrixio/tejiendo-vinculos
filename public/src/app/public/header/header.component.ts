import {Component, OnInit} from '@angular/core';
import {MatDialog,} from '@angular/material';
import {LoginComponent} from '../login/login.component';
import {RegistroComponent} from '../registro/registro.component';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public inicioSesionOff = true;

  constructor(public dialog: MatDialog, private toastr: ToastrService, private router: Router, private  route: ActivatedRoute) {
  }

  ngOnInit() {
    if (localStorage.getItem('user') == null) {
      this.inicioSesionOff = true;
    } else {
      this.inicioSesionOff = false;
    }
  }

  openDialoglogin(): void {
    const dialogRef = this.dialog.open(LoginComponent, {
      width: '40vw',
      height: '50vh',
      disableClose: true,
      data: this.inicioSesionOff
    });
    dialogRef.afterClosed().subscribe(result => {
      this.inicioSesionOff = result;
    });
  }

  openDialogregistrer(): void {
    const dialogRef = this.dialog.open(RegistroComponent, {
      width: '40vw',
      height: '94vh',
      disableClose: true,
      data: this.inicioSesionOff
    });
    dialogRef.afterClosed().subscribe(result => {
      this.inicioSesionOff = result;
    });
  }

  closesession() {
    if (localStorage.getItem('user') !== null) {
      localStorage.removeItem('user');
      this.inicioSesionOff = !this.inicioSesionOff;
      this.toastr.success('', 'La sesion se ha finalizado');
    }
  }

}
