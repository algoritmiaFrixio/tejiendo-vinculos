import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {LoginComponent} from '../login/login.component';
import {RegistroComponent} from '../registro/registro.component';

@Component({
  selector: 'app-alertainiciosesion',
  templateUrl: './alertainiciosesion.component.html',
  styleUrls: ['./alertainiciosesion.component.scss']
})
export class AlertainiciosesionComponent implements OnInit {


  constructor(public dialog: MatDialog,
              public dialogRef: MatDialogRef<AlertainiciosesionComponent>, @Inject(MAT_DIALOG_DATA) public data) {
  }

  ngOnInit() {
    document.getElementsByClassName('mat-dialog-container')[0].classList.add('register');
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  openDialoglogin(): void {
    const dialogRef = this.dialog.open(LoginComponent, {
      width: '66vw',
      height: '69vh',
      disableClose: true,
    });
    this.dialogRef.close();
  }

  openDialogregistrer(): void {
    const dialogRef = this.dialog.open(RegistroComponent, {
      width: '68vw',
      height: '95vh',
      disableClose: true,
    });
    this.dialogRef.close();
  }
}
