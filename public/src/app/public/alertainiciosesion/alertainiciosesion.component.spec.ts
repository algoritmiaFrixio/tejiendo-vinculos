import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertainiciosesionComponent } from './alertainiciosesion.component';

describe('AlertainiciosesionComponent', () => {
  let component: AlertainiciosesionComponent;
  let fixture: ComponentFixture<AlertainiciosesionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertainiciosesionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertainiciosesionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
