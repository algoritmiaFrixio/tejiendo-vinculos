import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ConsultaforoService} from '../../service/consulta-foro.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {CometaryComponent} from '../description/description.component';
import {Consulta1} from '../../datos/consulta';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.scss']
})
export class RegistroComponent implements OnInit {
  firstFormGroup: FormGroup;
  public icon = 'visibility_off\n';
  public type = 'password';
  public icon1 = 'visibility_off\n';
  public type1 = 'password';
  id;
  showcomments: Consulta1[] = [];

  constructor(private formBuilder: FormBuilder, public dialog: MatDialog,
              public dialogRef: MatDialogRef<RegistroComponent>, @Inject(MAT_DIALOG_DATA) public data,
              private consultaservice: ConsultaforoService, private toastr: ToastrService,
              private router: Router) {
    this.firstFormGroup = this.formBuilder.group({
      // tslint:disable-next-line:max-line-length
      nombre: ['', [Validators.required, Validators.pattern(/^([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\']+[\s])+([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\'])+[\s]?([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\'])?$/)]],
      // tslint:disable-next-line:max-line-length
      fecha_nacimiento: ['', Validators.required],
      telefono: ['', Validators.required],
      ciudad: ['', Validators.required],
      tipo_documento: ['', Validators.required],
      numero_documento: ['', Validators.required],
      correo: ['', [Validators.pattern(/^\w+([\.\+\-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/)]],
      contrasena: ['', Validators.required],
      confirmar_contrasena: ['']
    });
  }

  onNoClick(): void {
    this.dialogRef.close(true);
  }

  openDialogcomentario(): void {
    const dialogRef = this.dialog.open(CometaryComponent, {
      width: '45vw',
      height: '75vh',
      disableClose: true,
      data: {id: this.id, con: this.showcomments}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.showcomments = result;
      }
    });
  }

  ngOnInit() {
    document.getElementsByClassName('mat-dialog-container')[0].classList.add('register');
  }


  saveRegister() {
    if (this.firstFormGroup.value.contrasena !== this.firstFormGroup.value.confirmar_contrasena) {
      this.toastr.error('La contraseña que has ingreado no coincide con la anterior', 'Contraseña incorrecta');
    } else if (this.firstFormGroup.value.nombre === '' || this.firstFormGroup.value.apellido === ''
      || this.firstFormGroup.value.fecha_nacimiento === '' || this.firstFormGroup.value.telefono === ''
      || this.firstFormGroup.value.ciudad === '' || this.firstFormGroup.value.tippo_documento === '' ||
      this.firstFormGroup.value.numero_documento === '' || this.firstFormGroup.value.correo === ''
      || this.firstFormGroup.value.contrasena === '' || this.firstFormGroup.value.confirmar_contrasena === '') {
      this.toastr.warning('Verifique si ha introducido la informacion correspondiente en los campos',
        'Los campos no se han llenado correctamente');
    } else if (this.firstFormGroup.value.fecha_nacimiento > '1/01/2005') {
      this.toastr.warning('La fecha que has ingresado no es valida', 'La fecha que has ingresado no es valida');
    } else {
      this.consultaservice.savedataregister(this.firstFormGroup.value).subscribe(response => {
        this.firstFormGroup = response.datos;
        localStorage.setItem('user', btoa(JSON.stringify(response.datos)));
        if (localStorage.getItem('user') !== null) {
        } else {
        }
        this.toastr.success('Felicitaciones ha adquirido su menbresia', 'Haz completado correctamente el formulario de registro');
        this.dialogRef.close();
      }, error1 => {
        this.toastr.error(error1.error.media, 'Ha ocurrido un error al registrar el usuario')
      });
    }
  }

  public showPassword() {
    if (this.type === 'password') {
      this.icon = 'visibility';
      this.type = 'text';
    } else {
      this.icon = 'visibility_off\n';
      this.type = 'password';
    }
  }

  public showPassword1() {
    if (this.type1 === 'password') {
      this.icon1 = 'visibility';
      this.type1 = 'text';
    } else {
      this.icon1 = 'visibility_off\n';
      this.type1 = 'password';
    }
  }
}
