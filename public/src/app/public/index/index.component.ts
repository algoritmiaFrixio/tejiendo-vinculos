import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {ConsultaforoService} from '../../service/consulta-foro.service';
import {Consulta2} from '../../datos/consulta';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {SwiperComponent, SwiperConfigInterface} from 'ngx-swiper-wrapper';

export interface Swiper {
  url?: string;
  action?: string;
}

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {
  public type = 'component';
  public config: SwiperConfigInterface = {
    a11y: true,
    loop: true,
    direction: 'horizontal',
    slidesPerView: 1,
    mousewheel: true,
    scrollbar: false,
    navigation: true,
    pagination: false,
    preloadImages: false,
    keyboard: {
      enabled: true,
      onlyInViewport: false,
    },
    autoplay: {
      delay: 6000,
    },
    // Enable lazy loading
    lazy: true
  };
  public service;
  @ViewChild(SwiperComponent, {static: false, read: false}) componentRef?: SwiperComponent;
  // @ts-ignore
  // @ts-ignore
  public swipper: Swiper = [
    {
      url: 'https://api.reconocimientojusticiaydesarrollo.org/images/basic/03.png',
    },
    {
      url: 'https://api.reconocimientojusticiaydesarrollo.org/images/basic/04.png',

    },
    // {
    //   url: 'https://api.reconocimientojusticiaydesarrollo.org/images/basic/06.png',
    //
    // },
    {
      url: 'https://api.reconocimientojusticiaydesarrollo.org/images/basic/05.png',
      action: 'openDialog()'
    },
  ];

  public themes: string[] = [
    'https://api.reconocimientojusticiaydesarrollo.org/images/temas/1.png',
    'https://api.reconocimientojusticiaydesarrollo.org/images/temas/2.png',
    'https://api.reconocimientojusticiaydesarrollo.org/images/temas/3.png',
    'https://api.reconocimientojusticiaydesarrollo.org/images/temas/4.png',
  ];

  constructor(public dialog: MatDialog) {
  }

  ngOnInit() {
    setTimeout(() => {
      this.openDialog();
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogAcceptTYCComponent, {
      width: '605px',
      height: '605px',
    });
  }

  openDialogconsulta(): void {
    const dialogRef = this.dialog.open(DialogConsultaComponent, {
      width: '75vw',
      height: '65vh',
      disableClose: true,
    });
  }
}

@Component({
  selector: 'app-dialog-consulta',
  templateUrl: 'dialogconsulta.html',
  styleUrls: ['./dialogconsulta.css']
})
export class DialogConsultaComponent implements  OnInit{
  consul2: Consulta2[];
  consulta2: Consulta2 = {
    nombre: '',
    correo: '',
    recidencia: ''
  };

  constructor(
    public dialogRef: MatDialogRef<DialogConsultaComponent>, private consultaservice: ConsultaforoService,
    @Inject(MAT_DIALOG_DATA) public data) {
    consultaservice.get().subscribe(response => {
      this.consul2 = response.data;
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  savedate() {
    this.consultaservice.savedate(this.consulta2).subscribe(response => {
      this.consul2 = response.datos;
      this.consulta2.nombre = '';
      this.consulta2.correo = '';
      this.consulta2.recidencia = '';
    }, error1 => {
      console.error(error1);
    });
  }

  ngOnInit(): void {
    document.getElementsByClassName('mat-dialog-container')[0].classList.add('register');
  }
}

@Component({
  selector: 'app-dialogaccepttyc',
  templateUrl: 'dialog.accept.tyc.html',
})
export class DialogAcceptTYCComponent implements OnInit {
  tmp = false;

  constructor(
    public dialogRef: MatDialogRef<DialogAcceptTYCComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
    document.getElementsByClassName('mat-dialog-container')[0].classList.add('edd');
  }
}
