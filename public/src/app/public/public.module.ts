import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PublicRoutingModule} from './public-routing.module';
import {
  DialogAcceptTYCComponent,
  DialogConsultaComponent,
  IndexComponent
} from './index/index.component';
import { ForoComponent} from './foro/foro.component';
import {QuienessomosComponent} from './quienessomos/quienessomos.component';
import {GaleriaComponent} from './galeria/galeria.component';
import {CursoVirtualComponent} from './curso-virtual/curso-virtual.component';
import {ContactoComponent} from './contacto/contacto.component';
import { HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {VideosComponent} from './videos/videos.component';
import {ProgramacionComponent} from './programacion/programacion.component';
import {SWIPER_CONFIG, SwiperConfigInterface, SwiperModule} from 'ngx-swiper-wrapper';
import {AfroplayComponent} from './afroplay/afroplay.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {CometaryComponent, DescriptionComponent} from './description/description.component';
import {
  MatButtonModule,
  MatInputModule,
  MatDialogModule,
  MatCardModule,
  MatToolbarModule,
  MatDatepickerModule,
  MatNativeDateModule, MatSelectModule
} from '@angular/material';
import {RegistroComponent} from './registro/registro.component';
import {LoginComponent} from './login/login.component';
import {MatStepperModule} from '@angular/material/stepper';
import { AlertainiciosesionComponent } from './alertainiciosesion/alertainiciosesion.component';
import { RouterModule } from '@angular/router';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {NgxUiLoaderHttpModule, NgxUiLoaderModule} from 'ngx-ui-loader';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';


const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  observer: true,
  direction: 'horizontal',
  threshold: 50,
  spaceBetween: 5,
  slidesPerView: 1,
  centeredSlides: true
};


@NgModule({
  declarations: [
    IndexComponent,
    ForoComponent,
    QuienessomosComponent,
    GaleriaComponent,
    CursoVirtualComponent,
    ContactoComponent,
    HeaderComponent,
    FooterComponent,
    VideosComponent,
    ProgramacionComponent,
    AfroplayComponent,
    DescriptionComponent,
    DialogConsultaComponent,
    CometaryComponent,
    RegistroComponent,
    LoginComponent,
    AlertainiciosesionComponent,
    DialogAcceptTYCComponent,
  ],
  imports: [
    CommonModule,
    PublicRoutingModule,
    SwiperModule,
    FormsModule,
    HttpClientModule,
    MatButtonModule,
    MatInputModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatCardModule,
    MatToolbarModule,
    MatStepperModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    RouterModule,
    MatCheckboxModule,
    NgxUiLoaderModule,
    NgxUiLoaderHttpModule.forRoot({showForeground: true}),
    MatMenuModule,
    MatIconModule,
    MatSidenavModule

  ],
  entryComponents: [
    DialogConsultaComponent,
    CometaryComponent,
    RegistroComponent,
    LoginComponent,
    AlertainiciosesionComponent,
    HeaderComponent,
    DialogAcceptTYCComponent
  ],
  providers: [
    MatDatepickerModule,
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    }
  ]
})
export class PublicModule {
}
