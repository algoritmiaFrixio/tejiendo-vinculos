import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AfroplayComponent } from './afroplay.component';

describe('AfroplayComponent', () => {
  let component: AfroplayComponent;
  let fixture: ComponentFixture<AfroplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AfroplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AfroplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
