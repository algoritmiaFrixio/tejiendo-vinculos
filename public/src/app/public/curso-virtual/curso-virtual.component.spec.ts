import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CursoVirtualComponent } from './curso-virtual.component';

describe('CursoVirtualComponent', () => {
  let component: CursoVirtualComponent;
  let fixture: ComponentFixture<CursoVirtualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CursoVirtualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CursoVirtualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
