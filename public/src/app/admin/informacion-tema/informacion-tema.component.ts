import {Component, Inject, OnInit} from '@angular/core';
import {ConsultaforoService} from '../../service/consulta-foro.service';
import {ActivatedRoute} from '@angular/router';
import {Consulta1, Respuesta} from '../../datos/consulta';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {AdminConsultaService} from '../../service/admin-consulta.service';
import {ToastrService} from 'ngx-toastr';

export interface Con {
  nombre?: string;
  tema?: string;
  descripcion?: string;
}

export interface Res {
  nombre?: string;
  comentario?: string;
}

@Component({
  selector: 'app-informacion-tema',
  templateUrl: './informacion-tema.component.html',
  styleUrls: ['./informacion-tema.component.scss']
})
export class InformacionTemaComponent implements OnInit {
  public message = 'Descriocion de los temas';
  public message1 = 'Visualiza la actividad de los temas expuestos en al foro de discucion';
  consul;
  id;
  showcomments = [];
  fecha = '';
  reply: Res;
  showReply: Respuesta[] = [];

  constructor(public dialog: MatDialog, private consultaservice: ConsultaforoService, private route: ActivatedRoute,
              private adminConsulta: AdminConsultaService) {
    this.route.params.subscribe(params => {
      const id = +params.id;
      this.id = id;
      consultaservice.get().subscribe(response => {
        response.data.forEach(e => {
          this.consul = e.id === id ? e : this.consul;
        });
        this.showcomments = this.consul.comentarios;
      });
    });
  }

  openDialogReply(id): void {
    const dialogRef = this.dialog.open(DialogReplyeComponent, {
      width: '40vw',
      height: '60vh',
      disableClose: true,
      data: {id, rep: this.showReply}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.consul = {};
        this.showcomments = [];
        result.forEach(e => {
          this.consul = e.id === this.id ? e : this.consul;
        });
        this.showcomments = this.consul.comentarios;
      }
    });
  }

  ngOnInit() {
    this.fecha = new Date().getDate() + '/' + (new Date().getMonth() + 1) + '/' + new Date().getFullYear();
  }

}

export interface Rep {
  nombre?: string;
  respuesta?: string;
}

@Component({
  selector: 'app-dialog-reply',
  templateUrl: './respuesta-comentario/respuesta.html',
  styleUrls: ['./respuesta-comentario/respuesta.scss']
})

export class DialogReplyeComponent implements OnInit {
  consulta = {
    nombre: '',
  };
  respuesta: Respuesta = {
    nombre: '',
    respuesta: ''
  };

  constructor(public dialogRef: MatDialogRef<DialogReplyeComponent>, private adminConsulta: AdminConsultaService,
              @Inject(MAT_DIALOG_DATA) public data, private toastr: ToastrService, private route: ActivatedRoute) {
    this.respuesta.nombre = JSON.parse(atob(localStorage.getItem('user'))).nombre;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  SaveReply() {
    this.adminConsulta.SaveReply(this.respuesta, this.data.id).subscribe(response => {
      this.data.con = response.datos;
      this.respuesta.respuesta = '';
      this.dialogRef.close(this.data.con);
      this.toastr.success('Se ha agregado exitosamente', 'Respuesta agregado con exito');
    }, error1 => {
      this.toastr.error(error1.error.message, 'Ha ocurrido un error');
    });
  }

  ngOnInit(): void {
    document.getElementsByClassName('mat-dialog-container')[0].classList.add('delete');
  }
}
