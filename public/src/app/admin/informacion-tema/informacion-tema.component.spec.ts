import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformacionTemaComponent } from './informacion-tema.component';

describe('InformacionTemaComponent', () => {
  let component: InformacionTemaComponent;
  let fixture: ComponentFixture<InformacionTemaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformacionTemaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformacionTemaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
