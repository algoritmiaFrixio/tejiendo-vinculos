import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {IndexDashboardComponent} from './index-dashboard/index-dashboard.component';
import {TemasComponent} from './temas/temas.component';
import {DocumentosComponent} from './documentos/documentos.component';
import {FotosComponent} from './fotos/fotos.component';
import {VideosComponent} from './videos/videos.component';
import {InformacionTemaComponent} from './informacion-tema/informacion-tema.component';


const routes: Routes = [
  {
    path: '',
    component: IndexDashboardComponent
  },
  {
    path: 'tema',
    component: TemasComponent
  },
  {
    path: 'documento',
    component: DocumentosComponent
  },
  {
    path: 'foto',
    component: FotosComponent
  },
  {
    path: 'video',
    component: VideosComponent
  },
  {
    path: 'informacion/:id',
    component: InformacionTemaComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
