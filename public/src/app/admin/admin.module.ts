import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AdminRoutingModule} from './admin-routing.module';
import {IndexDashboardComponent} from './index-dashboard/index-dashboard.component';
import {NavbarComponent} from './component/navbar/navbar.component';
import {
  DialogConsultComponent,
  DialogDeleteComponent,
  DialogEdidComponent,
  TemasComponent
} from './temas/temas.component';
import {DocumentosComponent} from './documentos/documentos.component';
import {FotosComponent} from './fotos/fotos.component';
import {VideosComponent} from './videos/videos.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {HttpClientModule} from '@angular/common/http';
import {MatStepperModule} from '@angular/material/stepper';
import {RouterModule} from '@angular/router';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatSelectModule} from '@angular/material/select';
import {MatNativeDateModule} from '@angular/material/core';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {DialogReplyeComponent, InformacionTemaComponent} from './informacion-tema/informacion-tema.component';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from "@angular/material/list";


@NgModule({
  declarations: [
    IndexDashboardComponent,
    NavbarComponent,
    TemasComponent,
    DocumentosComponent,
    FotosComponent,
    VideosComponent,
    DialogConsultComponent,
    DialogEdidComponent,
    DialogDeleteComponent,
    InformacionTemaComponent,
    DialogReplyeComponent
  ],
  exports: [
    NavbarComponent,
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    MatCardModule,
    MatToolbarModule,
    HttpClientModule,
    MatButtonModule,
    MatInputModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatCardModule,
    MatToolbarModule,
    MatStepperModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    RouterModule,
    MatCheckboxModule,
    MatMenuModule,
    MatIconModule,
    MatSidenavModule,
    MatFormFieldModule,
    MatDividerModule,
    MatListModule
  ],
  entryComponents: [
    DialogConsultComponent,
    DialogEdidComponent,
    DialogDeleteComponent,
    DialogReplyeComponent
  ]
})
export class AdminModule {
}
