import {Component, Inject, OnInit} from '@angular/core';
import {ConsultaforoService} from '../../service/consulta-foro.service';
import {AdminConsultaService} from '../../service/admin-consulta.service';
import {ToastrService} from 'ngx-toastr';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

// Componente principal
@Component({
  selector: 'app-temas',
  templateUrl: './temas.component.html',
  styleUrls: ['./temas.component.scss']
})
export class TemasComponent implements OnInit {
  showconsult = [];
  fecha = '';
  public message = 'Añade un tema';
  public message1 = 'Agrega un nuevo tema en la pagina';

  constructor(public dialog: MatDialog, private consultaservice: ConsultaforoService) {
    consultaservice.get().subscribe(response => {
      this.showconsult = response.data;
    });
    this.fecha = new Date().getDate() + '/' + (new Date().getMonth() + 1) + '/' + new Date().getFullYear();
  }

  ngOnInit() {
  }

// crear tema
  openDialogConsulta(): void {
    const dialogRef = this.dialog.open(DialogConsultComponent, {
      width: '45vw',
      height: '75vh',
      disableClose: true,
      data: {con: this.showconsult}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.showconsult = result;
      }
    });
  }

  // editar tema
  openDialogEdit(id): void {
    const dialogRef = this.dialog.open(DialogEdidComponent, {
      width: '45vw',
      height: '50vh',
      disableClose: true,
      data: id
    });
    dialogRef.afterClosed().subscribe(result => {
      this.showconsult = result.foros;
    });
  }

  // Eliminat tema
  openDialogdelete(id): void {
    const dialogRef = this.dialog.open(DialogDeleteComponent, {
      width: '30vw',
      height: '25vh',
      disableClose: true,
      data: id
    });
    dialogRef.afterClosed().subscribe(result => {
      this.showconsult = result === undefined ? this.showconsult : result;
    });
  }
}

// Dialogo crear nuevo tema
@Component({
  selector: 'app-dialog-tema',
  templateUrl: 'nuevo-tema.html',
  styleUrls: ['./nuevo-tema.scss']
})
export class DialogConsultComponent implements OnInit {
  consul;
  consulta = {
    nombre: '',
    description: '',
    title: '',
  };

  constructor(public dialogRef: MatDialogRef<DialogConsultComponent>, private consultaservice: ConsultaforoService,
              @Inject(MAT_DIALOG_DATA) public data, private toastr: ToastrService) {
    this.consulta.nombre = JSON.parse(atob(localStorage.getItem('user'))).nombre;
  }

  // Metodo para cerrar el tema
  onNoClick(): void {
    this.dialogRef.close();
  }

// Metodo para guardar el tema creado
  savetema() {
    this.consultaservice.save(this.consulta).subscribe(response => {
      this.data.con = response.datos;
      this.consul = response.datos;
      this.dialogRef.close(this.data.con);
      this.toastr.success('Se ha agregado exitosamente', 'Tema agregado con exito');
    }, error1 => {
      console.error(error1);
    });
  }

  ngOnInit(): void {
    // Metodo para cambiar los estilos del dialogo
    document.getElementsByClassName('mat-dialog-container')[0].classList.add('tema');
  }
}

// Dialogo para editar tema
export interface Con {
  tema?: string;
  descripcion?: string;
}

@Component({
  selector: 'app-dialog-edid',
  templateUrl: './Edid-dialog/edit-dialog.html',
  styleUrls: ['./Edid-dialog/edid-dialog.scss']
})
export class DialogEdidComponent implements OnInit {
  public updateForm: FormGroup;
  showconsult;

  constructor(public dialogRef: MatDialogRef<DialogEdidComponent>, private consultaservice: ConsultaforoService,
              @Inject(MAT_DIALOG_DATA) public data, private toastr: ToastrService, public formBuilder: FormBuilder,
              private adminConsulta: AdminConsultaService) {
    this.updateForm = this.formBuilder.group({
      titulo_tema: ['', Validators.required],
      descripcion_tema: ['', Validators.required]
    });
    consultaservice.get().subscribe(response => {
      response.data.forEach(e => {
        this.showconsult = e.id === data ? e : this.showconsult;
      });
      this.updateForm.controls.titulo_tema.setValue(this.showconsult.tema);
      this.updateForm.controls.descripcion_tema.setValue(this.showconsult.descripcion);
    });
  }

  Edittema() {
    this.adminConsulta.editSolicitud(this.data, this.updateForm.value).subscribe(response => {
      this.dialogRef.close(response);
      this.toastr.success('Se ha editado el tema', 'Tema editado con exito');
    }, error1 => {
      this.toastr.error(error1, 'se se ha actualizado correctamente');
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
    document.getElementsByClassName('mat-dialog-container')[0].classList.add('edit');

  }

}

// Dialogo para eliminar tema
@Component({
  selector: 'app-dialog-delete',
  templateUrl: './delete-dialog/delete-dialog.html',
  styleUrls: ['./delete-dialog/delete-dialog.scss']
})

export class DialogDeleteComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DialogDeleteComponent>, private adminConsulta: AdminConsultaService,
              @Inject(MAT_DIALOG_DATA) public data, private toastr: ToastrService, public formBuilder: FormBuilder) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


  ngOnInit(): void {
    document.getElementsByClassName('mat-dialog-container')[0].classList.add('delete');
  }

  deleteService() {
    this.adminConsulta.DeleteTheme(this.data).subscribe(response => {
      this.dialogRef.close(response.data);
      this.toastr.success('Se ha eliminado el tema', 'Tema eliminado con exito');
    }, error1 => {
      this.toastr.error(error1, 'Algo ha salido mal');
    });
  }

}
