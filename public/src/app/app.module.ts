import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MainComponent} from './main/main.component';
import {HttpClientModule} from '@angular/common/http';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import {ToastrModule} from 'ngx-toastr';
import { NgxUiLoaderModule} from 'ngx-ui-loader';
import {MatSidenavModule} from '@angular/material/sidenav';
import { AdminMainComponent } from './admin-main/admin-main.component';
import {AdminModule} from './admin/admin.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    AdminMainComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        AngularFontAwesomeModule,
        ToastrModule.forRoot(),
        NgxUiLoaderModule,
        MatSidenavModule,
        AdminModule,
      ReactiveFormsModule,
      FormsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
